<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <formFactors>Large</formFactors>
    <label>Data.com Assessment</label>
    <logo>Clean_Assessment/Clean_Assessment_Tab.jpg</logo>
    <tab>Data_com_Assessment</tab>
    <tab>Assessment_Summary__c</tab>
    <tab>Waypoint__c</tab>
    <tab>Employment_Website__c</tab>
    <tab>Job_Posting__c</tab>
    <tab>Test_Parent__c</tab>
    <tab>Test_Child__c</tab>
    <tab>DataStore__c</tab>
    <tab>Debtor__c</tab>
    <tab>Creditor__c</tab>
    <tab>Sale__c</tab>
    <tab>Purchase__c</tab>
    <tab>Stock__c</tab>
    <tab>Cash__c</tab>
    <tab>Expense__c</tab>
    <tab>items_Google_Drive__x</tab>
</CustomApplication>
